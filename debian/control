Source: gridtools
Section: science
Priority: optional
Maintainer: Alastair McKinstry <mckinstry@debian.org>
Build-Depends: debhelper-compat (= 12), 
  dh-buildinfo,  
  ecbuild,
  gfortran | fortran-compiler,
  doxygen,
  mpi-default-dev,
  libboost-dev (>= 1.58.0)
Standards-Version: 4.5.0
Homepage: https://gridtools.github.io/gridtools/latest/index.html
Vcs-Browser: https://salsa.debian.org:/science-team/gridtools.git
Vcs-Git: https://salsa.debian.org:/science-team/gridtools.git

Package: libgridtools-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, 
 ecbuild, 
 mpi-default-dev,
 libboost-dev (>= 1.58.0)
Description: Framework for storage and computation on Weather and Climate Grids
 The GridTools (GT) framework is a set of libraries and utilities to develop
 performance portable applications in which stencil operations on grids are
 central.  It provides regular and block-structured grids commonly found in the
  weather and climate application field, addresses the challenges that arise
 from integration into production code, such as the expression of boundaryi
 conditions, or conditional execution. The framework is structured such that
 it can be called from different weather models or programming interfaces,
 and can target various computer architectures. This is achieved by separating
 the GT core library in a user facing part (frontend) and architecture specific
 (backend) parts. The core library also abstracts various possible data layouts
 and applies optimizations on stages with multiple stencils.
 The core library is complemented by facilities to interoperate with other 
 languages (such as C and Fortran), to aid code development and a communication
 layer.
